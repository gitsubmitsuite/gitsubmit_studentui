const Application = require('spectron').Application;
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = require('chai').expect;
const assert = require('chai').assert;
const path = require('path');
var fse = require("../node_modules/fs-extra");
var fs = require('fs');

var electronPath = path.join(__dirname, '..', 'node_modules', '.bin', 'electron');
if (process.platform === 'win32') electronPath += '.cmd'

const appPath = path.join(__dirname, '..');
global.before(function () {
  chai.should();
  chai.use(chaiAsPromised);
  fse.outputFile(require('os').homedir() + '\\AppData\\Roaming\\GitSubmit-Student\\config.json', '');
});

describe('Application testing', function () {
  let app;

  before(function () {

    app = new Application({

      path: electronPath,
      args: [appPath]
    });

    return app.start();
  });




  it('clicks for empty username and password', function () {

    var buttonClick = app.client.element('#index_login');
    buttonClick.click();
  });

  it('Waits for alerts to disappear', function (done) {
    setTimeout(done, 7000);
  });

  it('Writes empty string to Password field', function () {
    var inputID = app.client.element('#login_username');
    return inputID.setValue('msardar').then(function (value) {
      var buttonClick = app.client.element('#index_login');
      buttonClick.click();
    });

  });

  it('Waits for alerts to disappear', function (done) {
    setTimeout(done, 7000);
  });

  it('Provides wrong username and password', function () {
    var inputID = app.client.element('#login_username');
    return inputID.setValue('msardar1').then(function (value) {
      var inputPass = app.client.element('#login_password');
      return inputPass.setValue('gitsubmit').then(function (value) {
        var buttonClick = app.client.element('#index_login');
        buttonClick.click();
      });

    });


  });

  it('Waits for alerts to disappear', function (done) {
    setTimeout(done, 7000);
  });


  it('Provides correct username and password', function () {
    var inputID = app.client.element('#login_username');
    return inputID.setValue('msardar').then(function (value) {
      var inputPass = app.client.element('#login_password');
      return inputPass.setValue('gitsubmit').then(function (value) {
        var buttonClick = app.client.element('#index_login');
        buttonClick.click();
      });

    });


  });



  it('Waits for tutorial screen', function (done) {
    setTimeout(done, 10000);
  });

  it('Clicks on skip button', function () {

    var buttonClick = app.client.element('#tutorial_skip');
    buttonClick.click();
  })


  it('Waits for the git_interface to load', function (done) {
    setTimeout(done, 3000);
  });

  it('Expands the drawer', function () {
    var buttonClick = app.client.element('.mdl-layout__drawer-button');
    buttonClick.click();
  })

  // it('Expands clone repository', function () {
  //   var buttonClick = app.client.element('#clone_repository_anchor');
  //   buttonClick.click();
  // })

  // it('it inject JavaScript to the page', function () {
  //   app.client.selectorExecute("#clone_repository_anchor", function (anchor_tag) {
  //     anchor_tag.setAttribute('aria-expanded', 'true')
  //   });
  // });

  it('Waits for the click to open clone the repository section', function (done) {
    setTimeout(done, 5000);
  });



  // it('Selects a semester', function () {
  //   var buttonClick = app.client.element("li[data-nodeid='0']");
  //   buttonClick.click();


  // })

  // it('Waits for the selection of semester', function (done) {
  //   setTimeout(done, 5000);
  // });

  // it('Selects a course', function () {
  //   buttonClick = app.client.element("li[data-nodeid='1']");
  //   buttonClick.click();

  // })
  // it('Waits for the selection of course', function (done) {
  //   setTimeout(done, 5000);
  // });

  it('Selects a project', function () {
    buttonClick = app.client.element("li[data-nodeid='2']");
    buttonClick.click();

  })

  it('Waits for the selection of project', function (done) {
    setTimeout(done, 2000);
  });


  it('Selects a path to clone the project to', function () {
    var pathElement = app.client.element('#gitInterface_path')
    var dir = require('os').homedir() + "\\CLONETEST"

    if (fs.existsSync(dir)) {
      fse.removeSync(dir)
      fse.ensureDirSync(dir);
    }
    else {
      fse.ensureDirSync(dir);
    }
    return pathElement.setValue(dir).then(function (value) {
      it('Waits for the selection of project', function (done) {
        setTimeout(done, 2000);
      });

      var buttonClick = app.client.element('#gitInterface_clonebutton');
      buttonClick.click();
    })
  })

  it('Selects a project', function () {
    buttonClick = app.client.element("li[data-nodeid='2']");
    buttonClick.click();

  })

  it('Waits for the selection of project', function (done) {
    setTimeout(done, 2000);
  });
  it('Selects a project', function () {
    buttonClick = app.client.element("li[data-nodeid='2']");
    buttonClick.click();

  })
  it('Waits for the selection of project', function (done) {
    setTimeout(done, 2000);
  });
  it('Clones the same project again in same location', function () {
    var buttonClick = app.client.element('#gitInterface_clonebutton');
    buttonClick.click();
  })


  it('should create a file "Testing.txt" in folder', function () {
    var dir = require('os').homedir() + "\\CLONETEST\\iOS_Project_01"
    fse.outputFile(dir + '\\Testing.txt', 'Testing gitSubmit application is easy!');
  })

  it('Waits after the creation of the file', function (done) {
    setTimeout(done, 2000);

  });

  it('Should restart the app', function () {
    app = new Application({

      path: electronPath,
      args: [appPath]
    });

    return app.start();
  })


  it('Waits for the checking of file creation in cloned project', function (done) {
    setTimeout(done, 10000);

  });


  it('Should click on stage button', function () {

    var buttonClick = app.client.element('#gitInterface_stageFiles');
    buttonClick.click();
  })

  it('Waits for the stage click', function (done) {
    setTimeout(done, 2000);

  });

  it('Should enter commit message summary', function () {
    var commitMessage = app.client.element('#gitInterface_summaryTextArea')
    return commitMessage.setValue('Testing application commit').then(function (value) {
      it('Waits for the completion of the summary', function (done) {
        setTimeout(done, 2000);
      });
      var buttonClick = app.client.element('#gitInterface_commitChanges');
      buttonClick.click();

    })
  })

  it('Waits after the commit button is clicked', function (done) {
    setTimeout(done, 2000);
  });

  it('Clicks the upload button to push the changes', function () {
    var buttonClick = app.client.element('#gitInterface_uploadChanges');
    buttonClick.click();
  })
  it('Waits after the upload button is clicked', function (done) {
    setTimeout(done, 15000);

  });


  it('Should restart the app after push', function () {
    app = new Application({

      path: electronPath,
      args: [appPath]
    });

    return app.start();
  })


  it('Waits after the app restarts', function (done) {
    setTimeout(done, 15000);
  });

  it('Clicks the download button to pull the changes', function () {
    var buttonClick = app.client.element('#gitInterface_pullUpdates');
    buttonClick.click();
  })

  it('Returns true when the current window is focused', function () {
    return app.browserWindow.isFocused().should.eventually.be.true
  });

  it('Returns false when the window is not in full screen mode', function () {
    return app.client.browserWindow.isFullScreen().should.eventually.be.false
  });

  it('Should be done will all tests', function () {
    app.stop();
  })

  after(function (done) {
    // return app.stop();
    done();
  });


});













