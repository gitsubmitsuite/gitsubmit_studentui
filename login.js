const ipc = require('electron').ipcRenderer;
const Config = require('electron-config');
const config = new Config();






$(document).ready(function () {
  var login_success;
  var error_closed = true;
  var error_shown = false;
  function errorClosed() {

    error_Shown = false;
    error_closed = true;
  }
  function errorShown() {

    error_closed = false;
    error_Shown = true;
  }
  $('#login_password').keypress(function (e) {
    if (e.which == 13) {
      jQuery(this).blur();
      jQuery('#index_login').click();
    }
  });
  $("#index_login").click(function () {
    //alert("Login button clicked");

    var user = userName_Login();
    var pass = password_Login();
    if (user == "" && pass == "") {
      //alert("Username and Password can not be empty")
      if (error_closed) {
        showErrorAlert('Username and password can not be empty')
      }


    }
    else if (user == "" || pass == "") {
      if (error_closed) {
        showErrorAlert('Both username and password are required')
      }
    }
    else {

      login_success = ipc.sendSync('login', user, pass);
      if (login_success) {
        //alert('Hello '+user);
        ipc.send('show-tutorial')

      }
      else {
        showErrorAlert("Login was not successful, try again with different credentials")
      }

    }
  });

  function userName_Login() {
    return $("#login_username").val().trim();
  }

  function password_Login() {
    return $("#login_password").val();
  }

  function showErrorAlert(messageToBeShown) {
    $.notify({
      // options
      message: messageToBeShown
    }, {
        // settings
        type: 'danger',
        animate: {
          enter: 'animated shake',
          exit: 'animated slideOutRight'
        },
        onShown: errorShown,
        onClosed: errorClosed
      });
  }

  $('input').blur(function () {
    var $this = $(this);
    if ($this.val())
      $this.addClass('used');
    else
      $this.removeClass('used');
  });

  var $ripples = $('.ripples');

  $ripples.on('click.Ripples', function (e) {

    var $this = $(this);
    var $offset = $this.parent().offset();
    var $circle = $this.find('.ripplesCircle');

    var x = e.pageX - $offset.left;
    var y = e.pageY - $offset.top;

    $circle.css({
      top: y + 'px',
      left: x + 'px'
    });

    $this.addClass('is-active');

  });

  $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function (e) {
    $(this).removeClass('is-active');
  });

});
