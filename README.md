# GitSubmit_ StudentUI

This application uses basic framework prepared by the Electron

A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. 
- This is the app's **renderer process** are in 'xx.js' files
- css and js files for front-end of the application are i /css and /js folders respectively


# Install dependencies

* Clone this repository
`git clone https://S525061@bitbucket.org/gitsubmitsuite/gitsubmit_studentui.git`
* After cloning
`npm install`
* You can also use the dependencies as they are already built in the node_modules folder of the repository


# Owner of the repository

* Northwest Missouri State University

# Maintainers

* Mohammed, Sardar (mohammed_sardar@outlook.com)
* Samineni,Krishna Teja (krishna.samineni94@gmail.com)
* Kommirisetty,Prabhath Teja (prabhath_teja@outlook.com)

Important links for future use:

* [Distribution and packaging](http://electron.rocks/electron-angular-packing-and-distribution/)

* [Packaging instructions video on youtube](https://www.youtube.com/watch?v=IuYFDwPKE3c)

* [GitLab API](https://docs.gitlab.com/ee/api/README.html)

* [Nodegit API](http://www.nodegit.org/api/)

* [WebDriverIO API](http://webdriver.io/api.html)

* [Electron](http://electron.atom.io/docs/latest)

Credits:

* Nodegit repository for libgit2 [Nodegit](https://github.com/nodegit/nodegit)

* Node Git Lab repository for wrappers for GitLab API [node-gitlab](https://github.com/node-gitlab/node-gitlab)

* Bootstrap-TreeView styling repository [bootstrap-treeview](https://github.com/jonmiles/bootstrap-treeview)

* [Bootstrap notify](http://bootstrap-notify.remabledesigns.com/)

* [node-forge](https://github.com/digitalbazaar/forge)

* [keypair](https://github.com/juliangruber/keypair)

## Important points to remember for development

* The packaged app doesn't work with the SSH keys as the code manages it in the root folder but there is no root folder in packaged app. So we need to change the location where we store the keys in Backend libraries and how we are using for clone, push and pull

* The packages and distribution folders were not added to repository because for some reason, the git is not able to access these folder's files and stage them. They will be available on DVD provided

#### License [CC0 1.0 (Public Domain)](LICENSE.md)