const {Menu} = require('electron')
const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow
//Module for Electron-Config to manage a JSON file containing various system settings and user preferences
const Config = require('electron-config');
const config = new Config();
const userDataPath = app.getPath('userData')

//Module for path and url management
const request = require('request');
const path = require('path')
const url = require('url')

//Modules for function calls to backend Libraries
const clone = require('./gitsubmit_backend/Libraries/clone')
const auth = require('./gitsubmit_backend/Libraries/auth')
const list_projects = require('./gitsubmit_backend/Libraries/list-projects')
const status = require('./gitsubmit_backend/Libraries/status')
const add = require('./gitsubmit_backend/Libraries/add')
const commit = require('./gitsubmit_backend/Libraries/commit')
const log = require('./gitsubmit_backend/Libraries/log')
const create_ssh_keys = require('./gitsubmit_backend/Libraries/create_ssh_keys')
const push = require('./gitsubmit_backend/Libraries/push')
const pull = require('./gitsubmit_backend/Libraries/pull')
const open_repo = require('./gitsubmit_backend/Libraries/open_repo')
const Git = require('nodegit')
const fetch = require('./gitsubmit_backend/Libraries/fetch')

//IPC message communication declarations
const ipc = require('electron').ipcMain
const dialog = require('electron').dialog

//Variables for the file usage
var current_head;
var oid;
var indexHead;
var indexParent;
var allRepos = [];
var allLocalRepos = [];
let mainWindow
var show_git_interface;

// Variable to handle the menu options for the application on the menu bar
// Please see the file ./mainmenu.js for reference for menu configurations
//const {app, Menu} = require('electron')


//IPC (on) event handlers for messages received from renderer pages

//To perform git status
ipc.on('check-status', function (event) {
  checkStatusForCurrentRepo()
});

//For open-file-dialog event for folder selection for clone
ipc.on('open-file-dialog', function (event) {
  dialog.showOpenDialog({
    properties: ['openFile', 'openDirectory']
  }, function (files) {
    if (files) event.sender.send('selected-directory', files)
  })
})

//To show tutorial page
ipc.on('show-tutorial', function () {
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'tutorial.html'),
    protocol: 'file:',
    slashes: true
  }))
})

//To show git_Interface screen
ipc.on('show-git_Interface', function () {
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'git_Interface.html'),
    protocol: 'file:',
    slashes: true
  }))
  show_git_interface = true;
})

//To call the authentication Libraries and creation of ssh keys after login button is clicked
ipc.on('login', function (event, user, pass) {
  auth(user, pass, function (login_response) {
    event.returnValue = login_response;
    if (login_response) {
      create_ssh_keys();
    }
  });
})

//To get the current repo in config file
ipc.on('get_current_repo', function (event) {
  event.returnValue = config.get('current_local_repo')
})

//To populate all projects in clone projects list
ipc.on('load_projects', function (event) {
  list_projects(function (returnedProjects) {
    if (returnedProjects[0].list_project_error) {
      event.returnValue = false
    }
    else {
      event.returnValue = returnedProjects
    }
  });
})

//To clone a project when clone button is clicked and return the result
ipc.on('clone_project', function (event, repoUrl, folderDestination, project_name, projectID) {
  clone(repoUrl, folderDestination, project_name, function (response, messageObj) {
    if (response) {
      currentLocalRepo = folderDestination
      if (allRepos.indexOf(folderDestination) == -1) {
        allRepos.push({ 'localRepo': folderDestination + "\\" + project_name, 'remoteRepo': repoUrl, 'projectID': projectID })
        allLocalRepos.push({ folderDestination })
        config.set('allRepos', allRepos)
      }
      config.set('current_remote_repo', repoUrl)
      config.set('current_remote_repo_ID', projectID)
      config.set('current_local_repo', path.join(folderDestination + "\\" + project_name))
      if (config.get('current_local_repo')) {
        if (config.get('current_repo_head')) {
          current_head = config.get('current_repo_head')
        }
        else {
          current_head = '';
        }
        log(config.get('current_local_repo'), current_head, function (response, commitsObj, remoteCommitHeadObj, pull_flag, lastHeadObj) {
          if (response) {
            logObjectForUI = commitsObj
            config.set('current_repo_head', lastHeadObj.last_head)
            mainWindow.webContents.send('log_update', { 'commitsObj': commitsObj, 'remoteHead': remoteCommitHeadObj, 'pull_flag': pull_flag })
          }
        });
      }
    }
    mainWindow.webContents.send('open_repo_list_update', allLocalRepos, allRepos)
    event.returnValue = { response: response, message: messageObj.message, empty: messageObj.empty }
  });
})

//To execute when the stage button is clicked
ipc.on('stage_files', function (event, filesToBeStaged) {
  add(filesToBeStaged, config.get('current_local_repo'), function (response, stageObject) {
    if (response) {
      oid = stageObject.oid
    }
    event.returnValue = response;
  })
})

//To execute when commit button is clicked
ipc.on('commit_changes', function (event, summary, description) {
  var message = summary + "\n\n" + description
  var logObjectForUI;
  var commit_response;
  var commit_object;
  commit(config.get('current_local_repo'), oid, message, function (response, commitObj) {
    commit_response = response;
    commit_object = commitObj;
    if (response) {
      if (config.get('current_local_repo')) {
        if (config.get('current_repo_head')) {
          current_head = config.get('current_repo_head')
        }
        else {
          current_head = '';
        }
        log(config.get('current_local_repo'), current_head, function (response, commitsObj, remoteCommitHeadObj, pull_flag, lastHeadObj) {
          if (response) {
            logObjectForUI = commitsObj
            config.set('current_repo_head', lastHeadObj.last_head)
          }
          send_commit_response(response, commitsObj, remoteCommitHeadObj, pull_flag)
        });
      }

      function send_commit_response(response, commitsObj, remoteCommitHeadObj, pull_flag) {
        event.returnValue = { 'response': response, 'commitsObj': commitsObj, 'remoteHead': remoteCommitHeadObj, 'pull_flag': pull_flag };
      }
    }
    else {
      event.returnValue = { 'response': response, 'commitsObj': '', 'remoteHead': '', 'pull_flag': '' };
    }
  })
})

//To execute when the push button is clicked
ipc.on('push_changes', function (event) {
  var repo = config.get('current_local_repo')
  var pushUrl = config.get('current_remote_repo')
  push(pushUrl, repo, function (response) {
    event.returnValue = response
  })
})

//To execute when pull button is clicked
ipc.on('pull_changes', function (event) {
  var repo = config.get('current_local_repo')
  var pullUrl = config.get('current_remote_repo')
  pull(pullUrl, repo, function (response, messageObj) {
    if (response) {
      event.returnValue = { 'response': response, 'message': messageObj.message };
    }
    else {
      event.returnValue = { 'response': response, 'message': messageObj.message };
    }
  })
})

//To execute when the clone path event is received to format the path that was received from UI in order to clone
ipc.on('clone_path', function (event, path) {
  formatDirectory(path)
})

//Functions for usage in the file

//To check if the repo passed as an argument exists in the directory on machine
function local_repo_exists(localPath, fn) {
  open_repo(localPath, function (response) {
    fn(response);
  })
}

//To perform tasks when the app is launched
function onLaunch() {
  if (typeof config.get('allRepos') != 'undefined') {
    var repoList = config.get('allRepos');
    for (var i = 0; i < repoList.length; i++) {
      local_repo_exists(config.get('allRepos')[i].localRepo, function (response) {
        if (response == false) {
          config.delete('allRepos')[i]
        }
      })
    }
  }
  checkStatusForCurrentRepo()
}

//To check the status for current repo - perform git status and return the results on UI under changes to be staged
function checkStatusForCurrentRepo() {
  if (config.get('current_local_repo')) {
    local_repo_exists(config.get('current_local_repo'), function (response) {
      if (response) {
        status(config.get('current_local_repo'), function (response) {
          mainWindow.webContents.send('changes_update', { 'response': response })
        });
      }
      else {
        config.delete('current_local_repo');
        config.delete('current_remote_repo');
        config.delete('current_remote_repo_ID');
        mainWindow.webContents.send('app_repo_empty', { 'response': '' })
      }
    })
  }
  else {
    mainWindow.webContents.send('app_repo_empty', { 'response': '' })
  }
  if (config.get('current_local_repo')) {
    if (config.get('current_repo_head')) {
      current_head = config.get('current_repo_head')
    }
    else {
      current_head = '';
    }
    log(config.get('current_local_repo'), current_head, function (response, commitsObj, remoteCommitHeadObj, pull_flag, lastHeadObj) {
      if (response) {
        logObjectForUI = commitsObj
        config.set('current_repo_head', lastHeadObj.last_head)
        mainWindow.webContents.send('log_update', { 'commitsObj': commitsObj, 'remoteHead': remoteCommitHeadObj, 'pull_flag': pull_flag })
      }
    });
  }
}

//Creation of the window with code to decide which screen to show. First time login is mainly handled by this part of the code
function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({ width: 1250, height: 800 })
  show_git_interface = false;
  //If the user exists in config, show git_Interface screen
  if (config.get('loggedIn')) {
    show_git_interface = true;
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'git_Interface.html'),
      protocol: 'file:',
      slashes: true
    }))

  }
  else {
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'login.html'),
      protocol: 'file:',
      slashes: true
    }))
  }
  if (show_git_interface) {
    onLaunch();
    checkStatusForCurrentRepo();
  }
  mainWindow.on('focus', function () {
    if (show_git_interface) {
      checkStatusForCurrentRepo();
    }
  })

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

//To change the '\' to '/' for windows
function formatDirectory(folder) {
  if (process.platform == 'win32') {
    var change = folder.replace(/\\/g, '/')
  }
}

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})


app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})
