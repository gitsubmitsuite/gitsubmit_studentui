//Page respective implementations
const ipc = require('electron').ipcRenderer;
const Config = require('electron-config');
const config = new Config();


$(document).ready(function () {
	//Initial page loading stuff - disabling all functionalities and butttons
	// $('#gitInterface_pullUpdates').attr('disabled', 'disabled');
	// $('#gitInterface_uploadChanges').attr('disabled', 'disabled');
	// $('#gitInterface_stageFiles').attr('disabled', 'disabled');
	// $('#gitInterface_commitChanges').attr('disabled', 'disabled');
	$('#gitInterface_summaryTextArea').attr('disabled', 'disabled');
	$('#gitInterface_descriptionTextArea').attr('disabled', 'disabled');
	// $('#gitInterface_clonebutton').attr('disabled', 'disabled');
	// $('#commit_row').hide();

	// variable declarations.
	var assignmentSelected = false;
	var selectedAssignmentURL = "";
	var selectedAssignmentName = "";
	var selectedAssignmentID = "";
	var semester;
	var course;
	var project;
	var projects_response;
	var username = config.get("username");
	var staged = false;
	var stageFiles;
	var changesStaged;
	var remoteUrl;
	var clone_local_repo;
	var currentRepo = config.get('current_local_repo');
	var repo_list;
	var error_closed = true;
	var error_shown = false;
	var tree_openRepo = [];
	var tree_unstagedchanges = [];
	var tree_stagedchanges = [];
	var tree_commitchnages = [];
	if (typeof config.get('allRepos') != 'undefined') {
		repo_list = config.get('allRepos')
	}
	else {
		repo_list = [];
	}




	// Open repo section - choose cloned projects from open
	populate_open_repo()


	//Implementations to handle the display of the repo title in top-left area
	if (typeof currentRepo != "undefined") {
		var getCurrentRepo = ipc.sendSync('get_current_repo')
		console.log("Getting current repo:" + getCurrentRepo)
		$('#localRepo').html(slice_localRepo_name(getCurrentRepo))
		//$('#gitInterface_pullUpdates').removeAttr('disabled');
	}
	else {
		$('#gitInterface_pullUpdates').attr('disabled', 'disabled');
		$('#localRepo').html('No repository selected')
	}

	//Function call to load projects in clone list  on page load
	load_projects()



	if ($('#gitInterface_unstagedItems').has('li').length == 0) {
		$('#gitInterface_stageFiles').attr('disabled', 'disabled');
	}


	//Listen to changes_update message from main
	ipc.on('changes_update', function (event, data) {
		console.log(data.response)
		if (data.response.length != 0) {
			// $("#gitInterface_unstagedItems").children().remove();
			tree_unstagedchanges = [];
			for (var i = 0; i < data.response.length; i++) {
				var fileName = data.response[i].fileName;
				var status = data.response[i].status
				tree_unstagedchanges.push({ text: fileName, icon: "glyphicon glyphicon-file" })
				//$('#gitInterface_unstagedItems').append('<li name=\'' + fileName + '\'>' + fileName + '--' + status)
			}
			$('#gitInterface_stageFiles').removeAttr('disabled');

		}
		else {
			tree_unstagedchanges = [];
		}
		$('#gitInterface_unstagedChangesDiv').treeview({ data: tree_unstagedchanges, showBorder: false, selectedBackColor: '#4caf50' });
	});

	//Listen to update message from main for open repo list update
	ipc.on('open_repo_list_update', function (event, allLocalRepos, allRepos) {
		repo_list = allRepos
		populate_open_repo()
	})

	//Listen to message for no current repo from main
	ipc.on('app_repo_empty', function (event, responseObj) {
		$('#localRepo').html('No repository selected')
	})


	ipc.on('app_repo_empty_onLaunch', function (event, responseObj) {
		$('#localRepo').html('No repository selected')
	})

	//Listen to log-update message from main 
	ipc.on('log_update', function (event, data) {
		//console.log("IPC on log_update: with data:\n" + data.commitsObj[0].commitId)
		prepareCommitsList(data.commitsObj, data.remoteHead, data.pull_flag, function (response) {
			if (response) {
				console.log("log updated")
			}
		})
	})


	//Listen to selected-directory message from main in folder selection
	ipc.on('selected-directory', function (event, path) {
		document.getElementById("gitInterface_path").value = "";
		// document.getElementById("gitInterface_path").value = path;
		// $('#gitInterface_pullUpdates').removeAttr('disabled');
		$('#gitInterface_clonebutton').removeAttr('disabled');
		clone_local_repo = path[0]
		ipc.send('clone_path', clone_local_repo)
		document.querySelector('.mdl-textfield').MaterialTextfield.change(path);

	})
	//Button on-click functions

	//Stage button on-click function
	$("#gitInterface_stageFiles").click(function () {
		//alert("In Git Interface screen and Stage button is pressed");
		stageFiles = []
		tree_stagedchanges = []
		var stage_response = ipc.sendSync('stage_files', stageFiles)
		//alert("Stage:" + stage_response)
		$("#gitInterface_stagedChangesDiv ul.list-group").empty();
		if (stage_response) {

			$("#gitInterface_unstagedChangesDiv ul.list-group").children().each(function () {
				var unstagedItem = $(this).text()
				if ($("#gitInterface_stagedChangesDiv ul.list-group").children().length != 0) {
					$("#gitInterface_stagedChangesDiv ul.list-group").children().each(function () {
						var stagedItem = $(this).text()
						if (stagedItem != unstagedItem) {
							// $("#gitInterface_stagedItems").append("<li>" + unstagedItem + "</li>");
							//$('gitInterface_stagedItems').treeview
							tree_stagedchanges.push({ text: unstagedItem, icon: "glyphicon glyphicon-file" })
							//changesStaged.push(unstagedItem)
						}
					});
				}
				else {
					tree_stagedchanges.push({ text: unstagedItem, icon: "glyphicon glyphicon-file" })
					//changesStaged.push(unstagedItem)
				}

				$('#gitInterface_stagedChangesDiv').treeview({ data: tree_stagedchanges, showBorder: false, selectedBackColor: '#4caf50' });
			});
			$("#gitInterface_unstagedChangesDiv ul.list-group").children().remove();
			$('#commit_row').show();
			$('#gitInterface_summaryTextArea').removeAttr('disabled');
			$('#gitInterface_descriptionTextArea').removeAttr('disabled');
			$('#gitInterface_stageFiles').attr('disabled', 'disabled');
			showSuccessAlert("Successfully staged changes, please commit the changes")
		}
		else {
			showErrorAlert('Error staging changes')
		}
	});

	//Download button on-click function
	$("#gitInterface_pullUpdates").click(function () {
		var pull_response = ipc.sendSync('pull_changes');

		if (pull_response.response) {
			showSuccessAlert(pull_response.message)
			$("#gitInterface_pullUpdates").attr('disabled', 'disabled')
			$('ul#commits_to_pull').children().remove();
		}
		else {
			showErrorAlert(pull_response.message)
		}
	});

	//Clone button on-click function
	$("#gitInterface_clonebutton").click(function () {

		if (assignmentSelected == true && ($("#gitInterface_path").text != "")) {
			folderDestination = $('#gitInterface_path').val()
			var clone_response = ipc.sendSync('clone_project', selectedAssignmentURL, folderDestination, selectedAssignmentName, selectedAssignmentID);
			if (clone_response.response) {
				currentRepo = config.get('current_local_repo')
				$('#localRepo').html(slice_localRepo_name(currentRepo))
				assignmentSelected = false
				populate_open_repo()
				showSuccessAlert("Successfully cloned the repository")
				//$('#gitInterface_pullUpdates').removeAttr('disabled');
			}
			else {
				showErrorAlert(clone_response.message)
			}
		}
		else {
			showErrorAlert("Please select an assignment");
		}
	});

	//Commit button on-click function
	$("#gitInterface_commitChanges").click(function () {
		var description = "";
		if ($("#gitInterface_summaryTextArea").val() === "") {
			showErrorAlert("Please enter a summary at least in order to commit")
		}
		else {
			var summary = summary_textAreaRead();
			if ($("#gitInterface_descriptionTextArea").val() != "") {
				description = description_textAreaRead();
			}
			var commit_response = ipc.sendSync('commit_changes', summary, description)
			if (commit_response.response) {
				// for (i = 0; i < commit_response.logObj.length; i++) {
				// 	tree_commitchnages.push({ text: commit_response.logObj[i].commitId.slice(0, 7) });
				// }
				//$('#gitInterface_uploadChangesDiv').treeview({ data: tree_commitchnages, showBorder: false });
				console.log("Commit response: remoteHead:" + commit_response.remoteHead)
				prepareCommitsList(commit_response.commitsObj, commit_response.remoteHead, commit_response.pull_flag, function (response) {
					if (response) {
						$('#gitInterface_uploadChanges').removeAttr('disabled');
						showSuccessAlert("Commit successful, please push the changes on the remote repository")
					}
				})

			}
			else {
				showErrorAlert("Error commiting the staged changes")
			}

		}
	});

	//Upload button on-click function
	$("#gitInterface_uploadChanges").click(function () {
		$('#gitInterface_summaryTextArea').val('');
		$('#gitInterface_descriptionTextArea').val('');
		$('#gitInterface_uploadedChangesList').empty();
		$("#gitInterface_uploadChangesDiv ul.list-group").empty();
		$("#gitInterface_stagedChangesDiv ul.list-group").empty();
		$("#gitInterface_unstagedChangesDiv ul.list-group").empty();
		$('#gitInterface_uploadChanges').attr('disabled', 'disabled');
		$('#gitInterface_commitChanges').attr('disabled', 'disabled');
		$('#commit_row').hide();
		var push_response = ipc.sendSync('push_changes', remoteUrl);
		$('#gitInterface_stageFiles').attr('disabled', 'disabled');
		if (push_response) {
			showSuccessAlert("Successfully pushed the changes to the remote repository")
			$('ul#commits_to_push').children().remove();
		}
		else {
			showErrorAlert("Error pushing to the remote repository")
		}
	});




	$('#gitInterface_summaryTextArea').on('input', function (e) {

		if ($('#gitInterface_summaryTextArea').val() != "") {
			$('#gitInterface_commitChanges').removeAttr('disabled');
		}
		else {
			$('#gitInterface_commitChanges').attr('disabled', 'disabled');
			$('#gitInterface_uploadChanges').attr('disabled', 'disabled');
		}
	});

	$('#gitInterface_path').on('input', function (e) {
		if (($('#gitInterface_path').val() != "") && (assignmentSelected == true)) {
			//$('#gitInterface_pullUpdates').removeAttr('disabled');
			$('#gitInterface_clonebutton').removeAttr('disabled');
		}
		else {
			//$('#gitInterface_pullUpdates').attr('disabled', 'disabled');
			$('##gitInterface_clonebutton').attr('disabled', 'disabled');
		}

	});
	const selectDirBtn = document.getElementById('gitInterface_browsebutton')
	selectDirBtn.addEventListener('click', function (event) {
		ipc.send('open-file-dialog')
	})


	function path() {
		alert($("#gitInterface_path").val())
	}

	function summary_textAreaRead() {
		return $("#gitInterface_summaryTextArea").val()
	}
	function description_textAreaRead() {
		return $("#gitInterface_descriptionTextArea").val();
	}

	function populate_open_repo() {
		if (repo_list.length > 0) {
			tree_openRepo = []
			for (i = repo_list.length - 1; i >= 0; i--) {

				var slicedRepo = repo_list[i].localRepo.substring(repo_list[i].localRepo.lastIndexOf("\\") + 1)
				var project_path = repo_list[i].localRepo
				tree_openRepo.push({ text: slicedRepo + "<br><span name=\"location\">" + repo_list[i].localRepo + "</span>", ssh_url: repo_list[i].remoteRepo, location: repo_list[i].localRepo, projectID: repo_list[i].projectID })

			}
			$('#gitsubmit_treeOPenRepo').treeview({ data: tree_openRepo, showBorder: false, selectedBackColor: '#4caf50' });
		}
		else {
			tree_openRepo = []
			tree_openRepo.push({ text: 'It\'s lonely in here, clone some projects' })
			$('#gitsubmit_treeOPenRepo').treeview({ data: tree_openRepo, showBorder: false, selectedBackColor: '#4caf50' });
		}
	}
	//On node selected event handler for open repo node
	$('#gitsubmit_treeOPenRepo').on('nodeSelected', function (event, data) {
		// Your logic goes here
		console.log("Node selected in open repo")
		var remote_url = data.ssh_url
		var local_repo_location = data.location
		var projectID = data.projectID
		config.set('current_remote_repo', remote_url)
		config.set('current_local_repo', local_repo_location)
		config.set('current_remote_repo_ID', projectID)
		var getCurrentRepo = ipc.sendSync('get_current_repo')
		console.log("Project ID" + projectID)
		$('#localRepo').html(slice_localRepo_name(getCurrentRepo))
		ipc.send('check-status')
		//$('#gitInterface_pullUpdates').removeAttr('disabled');
	});

	//Implementations to handle error messages types and data
	//Different functions for error, information and success

	//Flags to make sure the alert is displayed once
	function errorClosed() {

		error_Shown = false;
		error_closed = true;
	}

	function errorShown() {

		error_closed = false;
		error_Shown = true;
	}

	//Error alert function
	function showErrorAlert(messageToBeShown) {
		$.notify({
			// options
			message: messageToBeShown
		}, {
				// settings
				type: 'danger',
				animate: {
					enter: 'animated shake',
					exit: 'animated slideOutRight'
				},
				onShown: errorShown,
				onClosed: errorClosed
			});
	}

	//Information alert function
	function showInformationAlert(messageToBeShown) {
		$.notify({
			// options
			message: messageToBeShown
		}, {
				// settings

				animate: {
					enter: 'animated slideInRight',
					exit: 'animated slideOutRight'
				},
				onShown: errorShown,
				onClosed: errorClosed
			});
	}

	//Success alert function
	function showSuccessAlert(messageToBeShown) {
		$.notify({
			// options
			message: messageToBeShown
		}, {
				// settings
				type: 'success',
				animate: {
					enter: 'animated slideInRight',
					exit: 'animated slideOutRight'
				},
				onShown: errorShown,
				onClosed: errorClosed
			});
	}

	//Trimming the repo name to show immediate root folder containing the repo
	function slice_localRepo_name(repo) {
		if (typeof repo != "undefined") {
			var startPoint = repo.lastIndexOf("\\")
			var slicedRepo = repo.substring(repo.lastIndexOf("\\", startPoint - 1))
			return slicedRepo
		}

	}

	//Implementations to load projects in the clone list
	function load_projects() {
		var treeObject = []
		projects_response = ipc.sendSync('load_projects');
		//console.log(projects_response)
		if (projects_response != false) {
			for (var i = 0; i < projects_response.length; i++) {
				semester = projects_response[i].split_namespace_result.semester
				course = projects_response[i].split_namespace_result.course
				project = projects_response[i].split_namespace_result.project
				console.log("Semester:" + semester + "COurse:" + course + "Project:" + project)
				if (i == 0) {
					treeObject.push({ text: semester, nodes: [{ text: course, nodes: [{ type: 'project', text: project, ssh_url: projects_response[i].ssh_url, id: projects_response[i].id }], selectable: false }], state: { expanded: false }, selectable: false })
				}
				else {
					for (var j = 0; j < treeObject.length; j++) {
						if (treeObject[j].text === semester) {
							//alert('Semester already there' + semester + " at pos:" + i)
							for (var k = 0; k < treeObject[j].nodes.length; k++) {
								if (treeObject[j].nodes[k].text === course) {
									//alert('Course already there' + course + " at pos:" + k)
									for (var l = 0; l < treeObject[j].nodes[k].nodes.length; l++) {
										if (treeObject[j].nodes[k].nodes[l].text != project) {
											//	alert("project not there")
											treeObject[j].nodes[k].nodes.push({ type: 'project', text: project, ssh_url: projects_response[i].ssh_url, id: projects_response[i].id })
										}
									}
								}
								else {
									treeObject[j].nodes.push({ text: course, nodes: [{ type: 'project', text: project, ssh_url: projects_response[i].ssh_url, id: projects_response[i].id }], selectable: false })
								}
							}
						}
						else {
							treeObject.push({ text: semester, nodes: [{ text: course, nodes: [{ type: 'project', text: project, ssh_url: projects_response[i].ssh_url, id: projects_response[i].id }], selectable: false }], state: { expanded: false }, selectable: false })
							//alert('Semester not there' + semester + " at pos:" + i)
						}
					}
				}
			}
			$('#tree').treeview({ data: treeObject, showBorder: false, selectedBackColor: '#4caf50' });

		}
		else {
			showErrorAlert('Error getting projects from server')
		}

	}

	//Node selected event handler for clone project selection
	$('#tree').on('nodeSelected', function (event, data) {
		// Your logic goes here

		if (data.type) {
			selectedAssignmentURL = data.ssh_url
			assignmentSelected = true;
			selectedAssignmentName = data.text
			selectedAssignmentID = data.id
			console.log("Git url is:" + data.ssh_url + " and name is:" + data.text + " and id is:" + data.id)
		}


	});

	//Implementations to handle the log output and show the commits to pull, push and also decide whether to show the download button or not
	function prepareCommitsList(commitsOnLocal, remoteCommitToPull, pull_flag, fn) {
		console.log(remoteCommitToPull)
		if (pull_flag) {
			$('#gitInterface_pullUpdates').removeAttr('disabled');
			showInformationAlert("Please pull the current version of repository from server")
		}
		else {
			$('#gitInterface_pullUpdates').attr('disabled', 'disabled');
		}
		if (remoteCommitToPull != '') {
			$('ul#commits_to_pull').children().remove();
			$('ul#commits_to_pull').append('<li><span class=\"pull_commitAuthor\">' + remoteCommitToPull.author_name + '</span><span class= "commitToPullNumber">' + remoteCommitToPull.short_id + '</span ><br><span class="commitTOPullMessage">' + remoteCommitToPull.title + '</span></li>')
		}
		else {
			$('ul#commits_to_pull').children().remove();
			$('ul#commits_to_pull').append('<li><span class=\"pull_commitAuthor\"></span><span class=\"commitToPullNumber\"></span><br><span class=\"commitTOPullMessage\"></span></li>')

		}
		if (commitsOnLocal != '') {


			$('ul#commits_to_push').children().remove();
			for (var i = 0; i < commitsOnLocal.length; i++) {
				$('ul#commits_to_push').append('<li><span class="push_commitAuthor">' + commitsOnLocal[i].authorName + '</span><span class= "commitToPushNumber">' + commitsOnLocal[i].commitId.slice(0, 7) + '</span ><br><span class="commitTOPushMessage">' + commitsOnLocal[i].message + '</span></li>')
			}
			$('#gitInterface_uploadChanges').removeAttr('disabled');
		}
		else {
			$('ul#commits_to_push').children().remove();
			$('ul#commits_to_push').append('<li><span class=\"push_commitAuthor\"></span><span class=\"commitToPushNumber\"></span><br><span class=\"commitTOPushMessage\"></span></li>')
			$('#gitInterface_uploadChanges').attr('disabled', 'disabled');
		}
		fn(true)

	}
});
