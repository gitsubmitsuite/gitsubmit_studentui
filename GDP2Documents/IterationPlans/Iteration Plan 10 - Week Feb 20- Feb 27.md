This page represents the Iteration Plan for the coming week of February 20 to February 27

## Current estimated Percentage of completion of project
At this point of time we have completed 60% of project.


# What was completed until now?
* Status functionality is completed
* Stage functionality is completed
* Commit functionality is completed
* git log functionality to list the commits is completed
* Library for push is completed, Integration with UI is still incomplete
* Sample test is completed with Spectron and mocha chai
* Packaging and building test was successful. We should be able to package and distribute with app completed 


## What didn't get done last week?
* Testing different components and sections of the app was not completed
* Documenting the official test plan was not completed


## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules
* Spectron with mocha chai


# What will be done this week?
* Modify the UI to accomodate opening a repo functionality
* Integrate push library with UI
* Integrate pull with UI
* Handle push for failure if the remote is ahead and has new commits
* Implement opening repo functionality
* Write library to create SSH keys within project space
* Write library for pull without handling merge conflict and alert for merge conflict and stop pull operation
* Write the test plan for GitSubmit Student team
* Write 15 integration tests


## What deliverable are involved?
* Submodule folder - push,pull,open for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js
*/tests/test.js


## How can it be tracked?
* Running application seeing the alerts
* Commits and changes pushed will be visible on remote repo
* Going to the destination repo folder to see if the project is pulled with latest commits
* Running tests by npm test command


## How does it get us closer to 100%?
* After push, we will focus on pull and merge.
* Testing phase has began and small tests are already being written.
* After this iteration, we should be at 65%. 

## Responsibilities and hours estimated?
* Krishna will be responsible writing a test plan and writing 15 integration tests. Estimated work hours is 15 hours. The stretch goal for him is to write 5 unit tests.

* Prabhath will be responsible for preparing the UI for open repo , integrating push and integrating pull. Estimated work hours is 15. The stretch goal for him is to integrate the diff tool with app if the library is ready to send the response.

* Sardar will be responsible for working with backend team and creating SSH keys in app space, handling push for failure if the remote is ahead and has new commits, library for opening a repo and library for pull. Estimated time is 15 hours. The stretch goal for him is to work on handling merge conflicts using a diff tool .