This page represents the Iteration Plan for the coming week of January 23 to January 29

## Current estimated Percentage of completion of project
At this point of time we have completed 40% of project.


# What was completed until now?
* User is able to login using Gitlab credentials
* List of Projects are in cmd
* Projects are in the list form instead of dropdown
* User dropdown for user to edit profile


## What didn't get done last week?
* The login functionality is done using code in main.js file. It needs to be imported or called from a library prepared by the backend

## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules

# What will be done this week?
* We are planning to sit with Back-end team on Wednesday, Friday and Sunday in order to make sure we have a good flow of work established
* Sub-module of Backend repo to be created and code flow to be established accordingly
* Login functionality to be done using Backend libraries and integrated with VM
* Testing different logins and testing using the VM
* List of projects to be populated in list using API on the UI, as currently it is only available on Backend
* Using same procedure to implement the functionality as done for login by communication with Backend libraries
* After the successful selection of a project, preparing the URL for cloning a project from UI and sending to Backend
* Working with Backend team to implement Clone functionality
* Implementing consistency between login page and git_interface page
* Implementing tooltip suggestions about various components on UI


## What deliverable are involved?
* Submodule folder for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js


## How can it be tracked?
* Config file shows the details of the user with private token and we may include a dialog to show it
* Once authenticated, the user will see only those projects which he/she are a member of. No hard-coded list means it is hitting the server
* If clone/download button is clicked, the project is available on the machine


## How does it get us closer to 100%?
* We are able to get to the server now, so that is a very important task and we are able to achieve it.
* Cloning a project would mean our application is able to execute Git commands which would be useful for other commands
* So after this week, we will be at 45% of the development cycle

## Responsibilities and hours estimated?

* Krishna will be responsible for Login functionality implemented with backend sub module, testing logins using the VM. Estimated work hours is 15 hours. The stretch goal for him is to research on OAuth authentication for clone and push operations 

* Prabhath will be responsible for populating the list on UI using back end library and communicating with backend team about the task. He will also be responsible for preparing the URL for the project to clone from the UI for the back end libraries. Estimated work hours is 15. The stretch goal for him is to create hierarchies of the projects/assignments and see how to populate the list

* Sardar will be responsible for communicating with back end team and integrating and working on cloning a project. This is a essential task for the application and important component for the project as it will establish a baseline of how to invoke Git commands in our application without using the CMD. Estimated time is 15 hours. The stretch goal for him is to make changes and then stage the changes using the application.