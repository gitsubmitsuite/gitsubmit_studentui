This page represents the Iteration Plan for the coming week of November 21 to December 04

## Current estimated Percentage of completion of project

At this point of time we have completed 20% of project.

# What was completed until now?

* Created JavaScript page for the login page.
* Created functionality for the buttons on the login page.
* Created functionality for the buttons on the git interface page.
* Created functionality for the buttons on the tutorial page. 


## What didn't get done last week?
* Not implemented functionality of collapsing a column on the git interface page after selecting a assignment and download button is clicked.
* Browse button which will open system directory page to select a folder is not able to select a folder.
* Rest of the work is in track.



## Resources needed
* Electron Documentation


# What will be done this week?

* Remaining tasks from the previous iteration will be completed this week.
* The complete application workflow is to be developed. (From Login screen to Main Screen)
* Changes suggested by the client are to included. Research on tutorial screen is to be done to make it more interesting like Git Kraken.
* Testing the application as a whole and see if every action is being applied.
* Developing in JavaScript the Git workflow by guiding the user through various components.
* Research on Electron framework to develop a way to remember the first install scenario.

## What deliverables are involved?
* JavaScript files
* CSS files
* HTML files
* Dependencies


## How can it be tracked?
* We can test the interaction of each screen and see if each component in the page is doing what it is designed for.
* We can test the transition from one screen to another.
* We can click on buttons and see if the are responding.
* We can enter text in input fields and see if they are being read.


## How does it get us closer to 100%?
* This is an assumption and it almost contributes to 5% of the total work involved in project
* So after this week, we will be at 25% of the development cycle


## Responsibilities and hours estimated?
* Prabhath Teja will be responsible for the testing and modifying the Tutorials page and Git_Interface screen - 20 hr.

* Krishna Samineni will be responsible for the testing and modifying the Login screen for initial authentication and the loading page during the authentication process - 20 hr.

* Mohammed Sardar will be researching on Electron framework to develop a way to remember the first install scenario and developing in JavaScript the Git workflow by guiding the user through various components. - 20 hr.
