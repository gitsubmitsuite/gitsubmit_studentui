This page represents the Iteration Plan for the coming week of January 16 to January 22

## Current estimated Percentage of completion of project
At this point of time we have completed 30% of project.


# What was completed until now?

* The configuration details and app information are accurate and as desired.
* Login page is improved.
* Research about GitLab API for authentication.
* Page navigation is implemented using dynamic ipc functionality with Node.js.
* Improved UI for gitInterface using MDL.


## What didn't get done last week?
* GitKraken tutorial idea research as there was work with current iteration
* The API is not ready for authentication but there was a meeting with Backend team in that regards. It is a goal for this week.
* Project selection component needed modification but had to be delayed because of the reorganization of the UI with MDL. Also we need to talk to client about an option regarding the look of the list.


## Resources needed
* GitLab VM to work on login
* Student volunteers for usability testing 


# What will be done this week?
* Plan for talking about arrangement of usability testing of the UI by a student
* Integrating with backend team for login functionality
* Providing the username and password for authentication and with the result providing either an error dialog or the tutorial page and ultimately the Git Interface.
* Implementing first install and first login scenario with tutorial, after that the app should run without prompt for login
* Adding a basic profile maintenance and logout UI components on the Git Interface


## What deliverable are involved?
* config file in JSON format to store variables
* backend js file with login API code
* Usability test plan
* Git Interface page with new components
* Fixed tutorial.html page


## How can it be tracked?
* We can run the app and see the outlook of the pages
* Config file is created in APPDATA directory on the running machine
* Login should appear only once and then it resumes with the Git Interface screen



## How does it get us closer to 100%?
* This is an assumption and it almost contributes to 10% of the total work involved in project.
* Login functionality will be the first real time functionality in our app. It will be a baseline of integration with back-end team.
* The first login and first install scenario is important part and it is one of the major goals for the project.
* So after this week, we will be at 40% of the development cycle.


## Responsibilities and hours estimated?
* Sardar will be responsible for Implementing first install and first login scenario with testing and working on including the library provided in right places. This should take 15 hours. The stretch goal for him will be to prepare the Usability test plan and prepare tasks and questions for user and maybe perform the usability test this week or next week.

* Krishna will be responsible for working with Backend team and implementing the Login functionality using GitLab API and integrating the required UI code with backend libraries and testing with UI. This should take 15 hours. The stretch goal for him is to make use of the Gitlab VM provided by the professor and create a user on it and then try login. 

* Prabhath will be responsible for tutorial page fix, select dropdown for projects list and introducing new components on GitInterface for logout and profile maintenance. This should be 15 hours of work. The Stretch goal is to populate the list by making use of backend code API and then selecting a project.