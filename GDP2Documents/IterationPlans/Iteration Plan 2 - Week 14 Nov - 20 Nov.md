This page represents the Iteration Plan for the coming week of November 14 to November 20

## Current estimated Percentage of completion of project

At this point of time we have completed 10% of project.

# What was completed until now?

Created screens with required elements

* Login screen
* Authentication Screen.
* Tutorial screen
* Main Interface screen with download,commit,upload buttons

## What didn't get done last week?
* Verification of the login screen, authentication screen and Git Interface screen according to the requirements
* Animation for the Git Cycle in Git Interface screen
* Components for the authentication screen including the animation


## Resources needed

* jQuery and jQuery UI library for front-end JavaScript development

 
# What will be done this week?

* Client demo, showing the progress and having a feedback on all HTML based developed screens
* Including the changes requested and then moving on with the JavaScript development
* Creating JavaScript files for each screen to be specific
* Creating JavaScript interaction with the HTML pages which involve button connections, screen transitions, feedback dialog, error messages and required functions
* Will be following the same methodology of assignment of single page responsibility to each team member


## What deliverables are involved?
* HTML pages 
* JavaScript files
* HTML linked to JavaScript code

## How can it be tracked?
* We can test the interaction of each screen and see if each component in the page is doing what it is designed for
* We can see if a button is created to download, it shows a dialog of download successful when it is clicked
* Open the Login screen and experience the transition from one screen to another until the Git Interface screen

## How does it get us closer to 100%?

* This is an assumption and it almost contributes to 10% of the total work involved in project
* So after this week, we will be at 20% of the development cycle


## Responsibilities and hours estimated?

* Prabhath Teja will be responsible for the JavaScript with respect to Tutorials page and Project list column in Git_Interface screen - 15 hr. Will be working on Issues #18, #21, #22, #26

* Krishna Samineni will be responsible for the JavaScript with respect to Login screen for initial authentication and the loading page during the authentication process - 15 hr. Will be working on Issues #19,#20,#24,#25

* Mohammed Sardar will be working on JavaScript development with respect to Git_Interface main page with all the components for Git process - 15 hr. Will be working on Issues #17,#23,#27,#28,#29,



