This page represents the Iteration Plan for the coming week of March 13 to March 20

## Current estimated Percentage of completion of project
At this point of time, we have completed 70% of the project.


# What was completed until now?
* Changed the error pop ups for better testing approaches.
* Login tests case completed.
* Login UI was improved.
* Tutorial UI was modified.
* Project selection is modified.
* Open repo UI design is modified.
* The application was packaged and tested with the discovery of few bugs.
* Commits are trimmed and shown on the UI.


## What didn't get done last week?
* The integration tests for clone and stage were not completed due to bugs in the application.

## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules
* Spectron with mocha chai


# What will be done this week?
* Fix the bugs in the application for status changes.
* Fix the bugs for project selection.
* Add code implementation to edit config's current repo and URL on opening of a repo[Issue #78](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/78/backlog-populate-the-recently-opened-repo) 
* Writing integration test for staging of changes [Issue #66](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/66/write-integration-tests-for-project)
* Determining if we can test alerts and writing respective integration test [Issue #73](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/73/backlog-determining-if-we-can-test-alerts)
* Determine if we can test collapsible content and test it if possible [Issue #72](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/72/backlog-determining-if-we-can-test-the)
* Determine if we can test for media content and test if possible [Issue #74](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/74/backlog-determining-if-we-can-test-for)
* Design the logo for GitSubmit for a better look.
* Modify the menu bar for the application.
* Improve the staged and unstaged changes area using Bootstrap-tree [Issue #62](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/62/backlog-improving-staged-and-unstaged)
* Writing integration test for cloning a project [Issue #67](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/67/backlog-write-integration-test-for-clone)
* Updated the issue to show remote commits [Issue #58](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/58/backlog-commits-trim-and-visualize-under)


## What deliverable are involved?
* Submodule folder - push,pull,open for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js
*/tests/test.js


## How can it be tracked?
* Now we have issue tracker to look for the resolution and any comments, bugs or issues.
* We can run the tests and see if they are passing.
* We can run the application and test for various interactions.
* On selecting the repo, we can see if it is selected.
* The list for status log will look different.


## How does it get us closer to 100%?
* After this iteration, we should be at 75%.
* There should be rigorous testing with the app and perform all the operations.
* Improving the UI and focussing on the look and feel. 
* Targeting for a Usability Test after Spring Break after all observed bugs are fixed over Spring Break.

## Responsibilities and hours estimated?
* Krishna will be responsible writing integration test for alerts, media content and collapsible content. He will also design the logo and add options in the menu bar for the application. Estimated work hours is 15 hours. The stretch goal for him is to test the app explicitly by running the app.exe file.

* Prabhath will be responsible for populating the commits list with remote commits, improving the changes list display using bootstrap-tree and writing an integration test for the clone. Estimated work hours is 15. Test adding the recent locations in opening a repo.

* Sardar will be responsible for fixing the bugs for status, fixing bugs for project selection, writing tests for stage changes. Estimated time is 15 hours. The stretch goal for him is to write the integration test for commits.