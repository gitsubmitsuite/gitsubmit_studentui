This page represents the Iteration Plan for the coming week of April 3 to April 9

## Current estimated Percentage of completion of project
At this point of time, we have completed 80% of the project.


# What was completed until now?
* App is polished for production delivery but will constitute the changes later after client meeting on Monday 10th April
* Menu has been prepared for the application menu bar
* Mini demo has been done with demonstration of all major tasks
* Documentation for testing has been prepared and will be integrated with the final documentation
* Some of the files have been cleared with unnecessary console logs


## What didn't get done last week?
* Testing is a bit behind due to the previous existing bugs
* Library for remote commits needs to be prepared will be taken care of this week


## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules
* Spectron with mocha chai


# What will be done this week?
* Polish the app for demo on Monday, 10th April 2017
* Clean files with logs and unnecessary code
* Write tests for clone, stage and commit
* Package the app for the demo
* Integrate a library for remote commits
* Prepare final documentation page with links to various other parts of the documentation
* Documentation on setting up the development environment
* Documentation on packaging and distribution
* Documentation for instructions on how to install app for student
* Instruction on how to use the app written in documentation on wiki
* Documentation for Testing by verifying the details with the mentor
* Documentation for library call structure with the backend libraries for future reference for mentor and client
* Documentation of open-source libraries used in Wiki and their accreditations.
* Sample video preparation for the tutorial help

## What deliverable are involved?
* Git_Interface - html and js files
* Login - html and js files
* main.js
*/tests/test.js
* Bitbucket Wiki


## How can it be tracked?
* We can run the tests and see if they are passing.
* We can run the application and test for various interactions.
* The application should run bug-free
* The pages will be available on Bitbucket repository

## How does it get us closer to 100%?
* After this iteration, we should be at 85%.
* There should be rigorous testing with the app and perform all the operations.
* Improving the UI and focussing on the look and feel. 
* Preparing for the client final demo of the full version of the app by April 10.

## Responsibilities and hours estimated?
* Krishna will be responsible writing integration test for clone, stage and commit, preparing final documentation page with links to various other parts of the documentation, documentation for Testing by verifying the details with the mentor and documentation for library call structures. Estimated work hours is 15 hours. The stretch goal for him is to write tests for push and pull.

* Prabhath will be responsible for cleaning of files, sample video for tutorial, documentation on how to use the app, writing an integration test for the clone, cleaning files and contribute to polishing the app. Estimated work hours is 15. Stretch goal for him to document and credit all the open-source projects that were used within the project under wiki.

* Sardar will be responsible writing library for remote commits and integrate remote commits features, polishing the app for demo, documentation on setting up dev environment, documentation for packaging and distribution, instruction on how to install app and packaging the app into executable. Estimated time is 15 hours. The stretch goal for him is to help backend team with library for remote commits.