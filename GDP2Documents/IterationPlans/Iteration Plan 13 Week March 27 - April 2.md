This page represents the Iteration Plan for the coming week of March 27 to April 2

## Current estimated Percentage of completion of project
At this point of time, we have completed 70% of the project.


# What was completed until now?
* Bugs are fixed for staging changes
* List to show status changes is modified using tree
* Logo is designed
* We have determined how to test the alerts and will be focussing on writing tests for that this week
* The collapsible content will be tested but it will be hidden
* Testing the media content is difficult and not a lot of resources available to refer to so we are keeping it on hold

## What didn't get done last week?
* The integration tests were not written and we should be back on track with testing this week

## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules
* Spectron with mocha chai


# What will be done this week?
* Polish the app for demo on Friday
* Clean files with logs and unnecessary code
* Write tests for clone, stage and commit
* Package the app
* Communicate with backend team to integrate a library for remote commits
* Add items in the menu bar for the application



## What deliverable are involved?
* Submodule folder - push,pull,open for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js
*/tests/test.js


## How can it be tracked?
* We can run the tests and see if they are passing.
* We can run the application and test for various interactions.
* The application should run bug-free


## How does it get us closer to 100%?
* After this iteration, we should be at 75%.
* There should be rigorous testing with the app and perform all the operations.
* Improving the UI and focussing on the look and feel. 
* Preparing for the client final demo of the full version of the app by April 9.

## Responsibilities and hours estimated?
* Krishna will be responsible writing integration test for alerts, collapsible content and modify the menu bar. He will also focus on documentation about the testing involved with our app. Estimated work hours is 15 hours. The stretch goal for him is to organize pages and sections for documentation by talking to the client.

* Prabhath will be responsible for communicating with backend team and populating the commits list with remote commits, writing an integration test for the clone, cleaning files and contribute to polishing the app. Estimated work hours is 15. Stretch goal for him to document and credit all the open-source projects that were used within the project under wiki.

* Sardar will be responsible writing tests for stage changes and commit changes, polishing the app for demo and packaging the app. Estimated time is 15 hours. The stretch goal for him is to help backend team with library for remote commits.