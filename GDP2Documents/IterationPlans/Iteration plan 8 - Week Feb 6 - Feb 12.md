This page represents the Iteration Plan for the coming week of February 6 to February 12

## Current estimated Percentage of completion of project
At this point of time we have completed 50% of project.


# What was completed until now?
* Clone functionality implemented using SSH.
* Talked to client regarding projects list namespace separation.
* Documentation on sequence of steps for various tasks like stage, commits, push, status, log etc done in wiki.
* Research on testing with electron posted on Wiki.
* Project hierarchy prepared
* Getting data from backend like ssh_url,semester, project,course etc.

## What didn't get done last week?
* After clone adding and staging changes is not done due to fix errors with clone and preparation of data for UI
* Test plan and a sample test not written due to discrepancies about documentation on Selenium for electron


## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules



# What will be done this week?
* Polishing project functionality until clone.
* Making clone work with dynamic URL from UI.
* Fixing bug with clone 
* Library to populate changes list with git status
* Library to stage changes using git add.
* Organize and clean Gitinterface.js file with unnecessary code and bad structure.
* Organize and clean main.js file with  with unnecessary code and bad structure.
* Comment all logs
* Dynamic list for git status rather than hardcoded lists
* Fixing and validating project selection
* Informational alerts on every operation
* Login page to be modified to keep consistency with application.
* Writing a test plan and sample test.
* Performing small tests on each component
* Performing test with application as a whole


## What deliverable are involved?
* Submodule folder - clone for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js


## How can it be tracked?
* Running application seeing the alerts
* Running tests
* Adding files in vm in a project and testing by cloning to see if they are downloaded
* If git status works, list will be more dynamic with files to be changed from project directory



## How does it get us closer to 100%?
* Clone works, it will be a baseline for the other commands like commit, add, log, push etc.
* This iteration focusses more on polishing for the demo and setting up the repo, cleaning code, fixing small bugs, making small changes etc.
* After this iteration, we should be at 55%. 

## Responsibilities and hours estimated?
* Krishna will be responsible for Research about testing with Selenium along with few functionalities for testing, other frameworks for Electron like Spectron and posting on Wiki, writing a test for login, testing small components of the applicationa and testing application as a whole. Estimated work hours is 15 hours. The stretch goal for him is to work on integrating stage changes from UI using backend modules and it should work with a project already on system.

* Prabhath will be responsible for orgaizing and structuring of files, code etc. Fixing the project selection bug, populating list for changes and alerts in the app for all operations. Estimated work hours is 15. The stretch goal for him is to integrate git status and populate all changes required in repo with backend and it should work with a project already on system.

* Sardar will be responsible for communicating with back end team and integrating and fixing bug for clone, making sure the clone is smooth on UI, add code library to git status and add code library to stage the changes. Estimated time is 15 hours. The stretch goal for him is to work with Krishna and Prabhath, make changes and then stage the changes using the application and see if it is staged using a gitbash.