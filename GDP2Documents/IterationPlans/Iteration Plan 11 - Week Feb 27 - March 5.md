This page represents the Iteration Plan for the coming week of February 27 to March 5

## Current estimated Percentage of completion of project
At this point of time we have completed 65% of project.


# What was completed until now?
* Push is integrated with UI and is being handled with errors
* Pull is integrated with UI
* Test plan was prepared
* Backlog was prepared and included in issue tracker 
* SSH keys are prepared globally in app space for each user
* Global repo option is completed and the application opens recently opened repo
* Library for pull is completed


## What didn't get done last week?
* We pretty much achieved the goals of the last iteration plan.

## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules
* Spectron with mocha chai


# What will be done this week?
* Handling merge conflicts by alerting the user about the problem to let them handle [Issue #60](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/60/backlog-pull-with-merge-conflicts)
* Testing pulling and pushing with all scenarios.
* Writing integration test for staging of changes [Issue #66](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/66/write-integration-tests-for-project)
* Improving the Project selection[Issue #61](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/61/backlog-improving-project-selection)
* Visualizing commits and trimming to show it on UI under changes to push [Issue #58](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/58/backlog-commits-trim-and-visualize-under)
* Writing integration test for cloning a project [Issue #67](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/67/backlog-write-integration-test-for-clone)
* Writing integration test for login [Issue #65](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/65/backlog-write-integration-tests-for-login)
* Improving login page to match the app theme [Issue #40](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/40/change-loginhtml-according-to-gitinterface)
* Determining if we can test alerts and writing respective integration test [Issue #73](https://bitbucket.org/gitsubmitsuite/gitsubmit_studentui/issues/73/backlog-determining-if-we-can-test-alerts)
## What deliverable are involved?
* Submodule folder - push,pull,open for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js
*/tests/test.js


## How can it be tracked?
* Now we have issue tracker to look for the resolution and any comments, bugs or issues
* We can run the tests and see if they are passing
* We can use the UI to test for merge conflict and opening the repo
* The list will be populated and we can see a real repo


## How does it get us closer to 100%?
* After this iteration we should be at 70%.
* There should be rigorous testing with the app and performing all the operations.

## Responsibilities and hours estimated?
* Krishna will be responsible writing a integration test for login, improving login and integration test for alerts. Estimated work hours is 15 hours. The stretch goal for him is to test the app explicitly by running the app.

* Prabhath will be responsible for population of the list in the UI for open repo ,trimming the commits and populating the commits list and writing integration test for clone. Estimated work hours is 15. Test adding the recent locations in opening a repo.

* Sardar will be responsible for handling merge conflict, writing tests for stage changes and testing pulling and pushing covering all scenarios. Estimated time is 15 hours. The stretch goal for him is to package the app as executable and test.