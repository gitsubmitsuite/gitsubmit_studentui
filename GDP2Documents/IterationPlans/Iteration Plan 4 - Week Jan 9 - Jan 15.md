This page represents the Iteration Plan for the coming week of January 09 to January 15

## Current estimated Percentage of completion of project
At this point of time we have completed 25% of project.


# What was completed until now?
* Developed very basic git user-interface for Student-UI.
* Login page for authenticating a user.
* Tutorial page. 
* Main Interface page - Divided the page into three sections.
    + First part browsing and selecting a project.
    + Staging,adding commits to a project and uploading a project.
    + Finally showing all recent changes to a project.


## What didn't get done last week?
* Until now everything is in track, as this is the beginning.



## Resources needed
* MDL dependencies
* Node.js node modules for dynamic page navigation


# What will be done this week?

* Make sure the project repository is up and current on everyone's system.
* The configuration details and app information is to be made accurate.
* Login page is to be improved to the desired skin.
* Research about GitLab API for authentication.
* Page transformation to be done using dynamic ipc functionality with Node.js.
* Design improvement research for the UI for gitInterface using MDL.
* Improve the project selection component to hierarchical list rather than just a select menu.

## What deliverable are involved?
* login page
* app config file
* git_interface page


## How can it be tracked?
* For app configuration details, we can check the config JSON file and check the name of the app and various details
* We can run the app and see the login page how it looks.
* Testing the page navigation from login to student UI
* On the student interface, see the hierarchical list of projects



## How does it get us closer to 100%?
* This is an assumption and it almost contributes to 5% of the total work involved in project.
* So after this week, we will be at 30% of the development cycle.
* We are just improvising things and it involves less addition of work.


## Responsibilities and hours estimated?
* Sardar will be looking at app configuration maintenance and page navigation. This should take up to 15 hours. Additionally if things are done within the less amount of time assigned, the next step will be start changing the skin of the Git Interface based on the research with MDL. 

* Krishna will be taking care of Login page re-design with better UI and changing the JavaScript code to jQuery. Research with GitLab API for authentication. This should take about 15 hours. Additionally if things are done within the less amount of time assigned,the next step will be to implement the API for authentication and prepare the UI aspect for backend team. 

* Prabhath will be taking care of Design improvement research for MDL and change of the select menu to hierarchical list. This should take about 15 hours. Additionally if things are done within the less amount of time assigned, the next step is to do research on Git Kraken tutorial idea and see if we can implement it in our app.


## For next week - WEEK 16 January - 22 January
* We are looking for better ideas for UI using MDL and may be changing few things and asking for feedback
* Plan for talking about arrangement of usability testing of the UI by a student
* Integrating with backend team for login functionality
* Implementing first install and first login scenario with tutorial, after that the app should run without prompt for login
* Implementing functionality for disabling workflow guidance for expert users
* Adding a basic profile maintenance and logout UI components on the Git Interface