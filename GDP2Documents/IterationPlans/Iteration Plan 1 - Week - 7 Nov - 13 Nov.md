This page represents the Iteration Plan for the coming week of November 7 to November 13

## Current estimated Percentage of completion of project

At this point of time we have completed 25% of the project which include

* Requirements Gathering
* Making the Design
* Creating prototypes for the design
* Creating documentation for the project

# What was completed until now?

* Until now we are done with
* Use cases for Student User Interface
* Prepared a SRS document, gives clear understanding of requirements
* Initial prototypes for student UI desktop application are created 

## What didn't get done last week?
 * All resources required are not gathered and installed into the systems, but we are almost done with that
And we are in track with rest of the work.


## Resources needed
1. Git ClientElectron 
2. Framework documentation
3. Node.js installed
4. Atom Text editor installed
5. Repository cloned
6. Mac machine
7. Linux machine
8. Windows machine

 
# What will be done this week?

* All the software and framework installations on all of team member's machines have to be taken care of.
* The basic UI screen design implementation in HTML has to begin for all the pages.
* Pages/Screens involved in the application have to be created.
* Javascript and Styling are to be done in coming weeks after.
* Each team member decided to take up the task to develop an individual page/screen.


## What deliverables are involved?
* Basic HTML pages for all screens.

## How can it be tracked?
* Planning to assign issues/tasks to each member and their respective progress can be tracked in repository.
* We can also see the progress on each screen by loading the page in browser as they are just HTML pages.

## How does it get us closer to 100%?
* This is an assumption and it almost contributes to 10% of the total work involved in project.
* This is a major task as these HTML pages will contain all the elements or components of the UI involved which will be shown to user for interaction with application.

## Responsibilites and hours estimated?
* Prabhath Teja will be responsible for the Tutorials page and Project list column in Git_Interface screen - 15 hr.
* Krishna Samineni will be responsible for the Login screen for initial authentication and the loading page during the authentication process - 15 hr.
* Mohammed Sardar will be working on Git_Interface main page with all the components for Git process - 15 hr.