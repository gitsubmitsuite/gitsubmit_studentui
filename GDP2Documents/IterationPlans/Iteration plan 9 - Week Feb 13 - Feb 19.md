This page represents the Iteration Plan for the coming week of February 13 to February 19

## Current estimated Percentage of completion of project
At this point of time we have completed 55% of project.


# What was completed until now?
* Status functionality implemented.
* Tested app functionality until clone.
* Clone works with dynamic URL from UI.
* Assignment on UI is selected and clones the appropriate selected projected in given folder
* Library to populate changes list with git status created
* Organized and cleaned files
* Login page polished for demo

## What didn't get done last week?
* Staging changes is not done but should be done by Wednesday's client meeting
* Test plan and a sample test not written due to installation problems for Spectron

## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules


# What will be done this week?
* Library to stage changes using git add.
* Integrate backend library for stage with UI.
* Package and build an executable for testing app with nodegit
* Implement git commit library and integrate with UI 
* Writing a test plan and sample test.
* Performing small tests on each component
* Performing test with application as a whole


## What deliverable are involved?
* Submodule folder - stage for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js


## How can it be tracked?
* The application will start by running the .exe file
* Running application seeing the alerts
* If git status works, list will be more dynamic with files to be changed from project directory
* Going to the destination repo folder to see if the project is cloned
* Running tests


## How does it get us closer to 100%?
* We are getting closer to pushing changes, only add and commit remains which shouldn't be a big problem.
* Testing phase should begin this week as we were successful in installing Spectron in class.
* After this iteration, we should be at 60%. 

## Responsibilities and hours estimated?
* Krishna will be responsible writing a test for login, testing small components of the applications and testing application as a whole. Estimated work hours is 15 hours. The stretch goal for him is to integrating commit.

* Prabhath will be responsible for integrating git status and populate all changes required in repo with backend and it should work with a project already on system. Estimated work hours is 15. The stretch goal for him is to integrate stage

* Sardar will be responsible for Package and building an executable for testing app with nodegit. Estimated time is 15 hours. The stretch goal for him is to work with backend on developing libraries for stage and commit.