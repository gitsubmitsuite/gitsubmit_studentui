This page represents the Iteration Plan for the coming week of January 30 to February 05

## Current estimated Percentage of completion of project
At this point of time we have completed 45% of project.


# What was completed until now?
* Integrated with Backend and added submodule with libraries
* List of Projects are on the UI side with the use of backend libraries
* URL for clone is prepared from the UI side
* Login with backend libraries
* Built and installed nodegit for Electron 1.4.1


## What didn't get done last week?
* Clone functionality was not successful due to issue with authentication
* Hierarchies in project list and testing URL links for clone
* After clone adding and staging changes is not done


## Resources needed
* GitLab VM
* Dummy projects
* Logins
* Backend submodules
* nodegit SSH handlers


# What will be done this week?
* Clone functionality to be implemented using SSH and nodegit Integrated into backend module
* Talking to client and asking about current class-project-assignment setup in GitLab and reflecting that on UI
* Working on bare gitbash and writing documentation on sequence of steps for various tasks like stage, commits, push, status, log etc.
* Research on testing with Electron and writing a test plan


## What deliverable are involved?
* Submodule folder for backend library
* Git_Interface - html and js files
* Login - html and js files
* main.js


## How can it be tracked?
* If clone/download button is clicked, the project is available on the machine
* Modifying the projects in VM server similar to what it is currently on GitLab and seeing the results on UI
* Steps can be included in Wiki and even screenshots
* Research about testing on Wiki under a new page testing


## How does it get us closer to 100%?
* We were able to install nodegit, so this means git commands should be able to run on our app without cmd involved
* If clone works, it will be a baseline for the other commands like commit, add, log, push etc.
* After this iteration, we should be at 55%.

## Responsibilities and hours estimated?
* Krishna will be responsible for Research about testing with Selenium along with few functionalities for testing, other frameworks for Electron like Spectron and posting on Wiki. Also, writing the steps required for commands in gitbash. Estimated work hours is 15 hours. The stretch goal for him is to work on integrating stage changes from UI using backend modules and it should work with a project already on system.

* Prabhath will be responsible for populating the list on UI with class-project-assignment hierarchy and preparing the URL accordingly by communicating with the backend team. Estimated work hours is 15. The stretch goal for him is to integrate git status and populate all changes required in repo with backend and it should work with a project already on system.

* Sardar will be responsible for communicating with back end team and integrating and working on cloning a project using SSH. This is a essential task for the application and important component for the project as it will establish a baseline of how to invoke Git commands in our application without using the CMD. Estimated time is 15 hours. The stretch goal for him is to work with Krishna and Prabhath, make changes and then stage the changes using the application and see if it is staged using a gitbash.